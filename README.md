# Object-Centred Euphoria

Object-centred programming - a new paradigm for computer programming.

What is Object-Centred Programming?
Are you a person, like me, who wishes there was a programming paradigm between OOP and procedural? If so, read on!

Object-Centred Programming is a concept which, like OOP, focusses on "object"s, and manipulates these using routines (designed to work for specific object types), but which leaves a programmer free to organise his/her program, or libraries, in ways which suit themselves.

What is on offer here?
This site will eventually contain

* a wiki which explains the concept in outline
* a document outlining the concept
* a fully-defined (interpreted) language specification for an implementation of OCP
* a core language set
* a set of auxiliary modules (libraries adding extra functionality, if required)
* details of how to invoke the interpreter and how to create executables for distribution
* a set of test files with details of how to run them
## The development process

The project will be developed dynamically, so frequent updates will take place. To follow progress keep watching this project!

Contributions
If you would like to join the team then see below for how to do so.

Who do I talk to?
Bitbucket user: CANewbould


File details
Last updated
2017‑07‑30
Lines
32
Size
1.27 KB

0 builds

Give feedback