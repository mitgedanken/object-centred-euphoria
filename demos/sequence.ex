--------------------------------------------------------------------------------
--  sequence.ex
--------------------------------------------------------------------------------
-- Test bed for routines defined for the sequence type.
--------------------------------------------------------------------------------
--Version 4.1.0.64.9
--Author: C A Newbould
--Date: 2020.08.03
--Status: incomplete; operational
--Changes:]]]
--* added test for ##matchIn##
--------------------------------------------------------------------------------
include object.e    --v4.1.0.64.8
--------------------------------------------------------------------------------
constant PROMPT = "The sequence is ", REVERSE = "After reversing; ",
            DOTS = "......", ENTER = "Enter a string: ",
            ENTERED = "You entered: ", QUOTE = '"',
            HEADING = "The first element is: ",  --V4.1.0.64.3
            TRAILING = "The last three elements are: ",  --V4.1.0.64.3
            REMOVE = "Removing ",  --V4.1.0.64.7
            FROM = " from ",  --V4.1.0.64.7
            RESULTSIN = " results in : ",  --V4.1.0.64.7
            INSTS = "all instances of "  --V4.1.0.64.7
--------------------------------------------------------------------------------
procedure test(sequence this, integer start=1, integer stop=0)  --v4.1.0.64.4
    write(PROMPT)
    show(this)
    write(REVERSE & PROMPT)
    show(reverse(this, start, stop))
    if lengthOf(this) then  --V4.1.0.64.3
        write(HEADING)  --V4.1.0.64.3
        show(partOf(this))  -- defaults (HEAD, 1)   --V4.1.0.64.3
    end if  --V4.1.0.64.3
    if lengthOf(this) > 2 then  --V4.1.0.64.3
        write(TRAILING) --V4.1.0.64.3
        show(partOf(this, TAIL, 3)) --V4.1.0.64.3
    end if  --V4.1.0.64.3
    object found = findIn(this, 7, start)  --V4.1.0.64.5    --V4.1.0.64.6
    write("Finding 7 in the source: ")  --V4.1.0.64.5
    string not_found = "not found"  --V4.1.0.64.5
    if atom(found) then
        if found then  --V4.1.0.64.5
            show(found, "%d\n")  --V4.1.0.64.5
        else  --V4.1.0.64.5
            write(not_found, LF)  --V4.1.0.64.5
        end if  --V4.1.0.64.5
    else    --V4.1.0.64.6
        show(found)
    end if    --V4.1.0.64.6
    write(DOTS, LF)
end procedure
--------------------------------------------------------------------------------
procedure getString()
    write(ENTER)
    string str = String()
    write()   -- equivalent to writeln() in some languages  --V4.1.0.64.3
    write(ENTERED & QUOTE & str & QUOTE)  --removed LF: V4.1.0.64.3
    if lengthOf(str) > 3 then   --V4.1.0.64.3 & v4.1.0.64.7
        write("\nThe extract [2..4] is: " & partOf(str, 2, 3), LF)--V4.1.0.64.3  --v4.1.0.64.4
    else -- v4.1.0.64.7
        write(" - too short for an extract", LF) -- v4.1.0.64.7
    end if--V4.1.0.64.3
end procedure
--------------------------------------------------------------------------------
procedure test_remove() -- v4.1.0.64.7
    write(DOTS, LF)   -- V4.1.0.64.7
    write(REMOVE)   -- V4.1.0.64.7
    integer rem = 4   -- V4.1.0.64.7
    show(rem, "%dth")   -- V4.1.0.64.7
    write(FROM)   -- V4.1.0.64.7
    string s = "Johnn Doe"  --V4.1.0.64.7
    write(s)  --V4.1.0.64.7
    write(RESULTSIN)  --V4.1.0.64.7
    write(remove(s, rem), LF)  --V4.1.0.64.7
    write(REMOVE)  --V4.1.0.64.7
    write(INSTS)  --V4.1.0.64.7
    show(rem, "%d")   -- V4.1.0.64.7
    write(FROM)   -- V4.1.0.64.7
    sequence seq = {1,2,3,3,4,4,4,5}
    show(seq)  --V4.1.0.64.7
    write(RESULTSIN)  --V4.1.0.64.7
    show(removeFrom(seq, rem))  --V4.1.0.64.7
end procedure   --v4.1.0.64.7
--------------------------------------------------------------------------------
procedure main(sequence heading) --v4.1.0.64.4
    write(repeat('-', lengthOf(heading)), LF) --Using Core routine v4.1.0.64.4
    write(heading, LF) --v4.1.0.64.4
    write(repeat('-', lengthOf(heading)), LF) --Using Core routine v4.1.0.64.4
    test({1,3,5,7})  --v4.1.0.64.4
    test({1,3,5,7,9}, 2, -1)  --v4.1.0.64.4
    test({1,3,5,7,9}, 2)  --v4.1.0.64.4
    test({{1,2,3}, {4,5,6}})  --v4.1.0.64.4
    test({99})  --v4.1.0.64.4
    test(EMPTY_SEQUENCE)  --v4.1.0.64.4
    test(repeat(7,4), ALL)
    getString()  --v4.1.0.64.4
    test_remove()   -- v4.1.0.64.7
    string s = "I am the way, the truth and the light!"
    write("Sentence: ")
    write(s, LF)
    write("Find the comma in the string; at position ")-- v4.1.0.64.9
    ?matchIn(s, ",") -- v4.1.0.64.9
    write("Find all the 'the's in the string: ")-- v4.1.0.64.9
    ?matchIn(s, "the", ALL) -- v4.1.0.64.9
    write("\n*** Enter any key to close app *** ", WAIT) --v4.1.0.64.4 & v4.1.0.64.7
end procedure   --v4.1.0.64.4
--------------------------------------------------------------------------------
--  Execution
--------------------------------------------------------------------------------
main("Testing sequence routines") --v4.1.0.64.4
--------------------------------------------------------------------------------
--  Previous versions
--------------------------------------------------------------------------------
--Version 4.1.0.64.8
--Author: C A Newbould
--Date: 2019.12.21
--Status: incomplete; operational
--Changes:]]]
--* modified include
--------------------------------------------------------------------------------
--Version 4.1.0.64.7
--Author: C A Newbould
--Date: 2019.12.08
--Status: incomplete; operational
--Changes:]]]
--* ##remove## test added
--* ##removeFrom## test added
--* ##getString## modified
--------------------------------------------------------------------------------
--Version 4.1.0.64.6
--Author: C A Newbould
--Date: 2019.12.03
--Status: incomplete; operational
--Changes:]]]
--* ##findIn## test extended
--------------------------------------------------------------------------------
--Version 4.1.0.64.5
--Author: C A Newbould
--Date: 2019.12.01
--Status: incomplete; operational
--Changes:]]]
--* ##findIn## tested
--------------------------------------------------------------------------------
--Version 4.1.0.64.4
--Author: C A Newbould
--Date: 2019.11.29
--Status: incomplete; operational
--Changes:]]]
--* ##test## renamed
--* ##test## utilised in a different way
--------------------------------------------------------------------------------
--Version 4.1.0.64.3
--Author: C A Newbould
--Date: 2019.11.28
--Status: incomplete; operational
--Changes:]]]
--* ##partOf## tested
--* ##getString## modified
--------------------------------------------------------------------------------
--Version 4.1.0.64.2
--Author: C A Newbould
--Date: 2019.11.12
--Status: incomplete; operational
--Changes:]]]
--* ##String## tested 
--------------------------------------------------------------------------------
--Version 4.1.0.64.1
--Author: C A Newbould
--Date: 2019.11.12
--Status: incomplete; operational
--Changes:]]]
--* ##EMPTY_SEQUENCE## used 
--------------------------------------------------------------------------------
--Version 4.1.0.64.0
--Author: C A Newbould
--Date: 2019.11.11
--Status: incomplete; operational
--Changes:]]]
--* created
--* ##reverse## tested 
--------------------------------------------------------------------------------
