--------------------------------------------------------------------------------
--  object.ex
--------------------------------------------------------------------------------
-- Test bed for routines defined for the object type.
--
-- Tested on the following:
-- MS Windows
--  OE4.1.0(64)
--  OE4.1.0(32)
--  OE4.0.5
-- Linux
--	OE4.1.0(64)
--------------------------------------------------------------------------------
--Version 4.1.0.64.7
--Author: C A Newbould
--Date: 2020.05.03
--Status: complete; operational
--Changes:]]]
--* modified for Linux
--------------------------------------------------------------------------------
include object.e
--------------------------------------------------------------------------------
procedure test(object this) --v4.1.0.64.3
    show(this)
end procedure
--------------------------------------------------------------------------------
procedure main(sequence heading) --v4.1.0.64.3
    string header = repeat('-', lengthOf(heading)) --Using Core routine  --v4.1.0.64.5
    show(header, "%s\n") --v4.1.0.64.5
    write(heading, LF) --v4.1.0.64.5
    show(header, "%s\n") --v4.1.0.64.5
    test(7) --v4.1.0.64.3
    test({1,2,3,4,5}) --v4.1.0.64.3
    test(12345.67) --v4.1.0.64.3
    test("Hello, there!") --v4.1.0.64.3
--need a pause here - for Linux anyway
	ifdef LINUX then
		write("Look at the output before continuing, " &
					"as the message box following will obsure them!", WAIT)
	end ifdef
    integer resp = write("Close me when you have checked the output!", TOABOX, heading, OKCANCEL)      --v4.1.0.64.5
    if resp = OKED then   --v4.1.0.64.4
        write("You chose OK\n\nClose to exit", TOABOX, "Reply")   --v4.1.0.64.5
    end if   --v4.1.0.64.4
end procedure   --v4.1.0.64.3
--------------------------------------------------------------------------------
--  Execution
--------------------------------------------------------------------------------
main("Testing object routines") --v4.1.0.64.3
--------------------------------------------------------------------------------
--  Previous versions
--------------------------------------------------------------------------------
--Version 4.1.0.64.6
--Author: C A Newbould
--Date: 2020.02.03
--Status: complete; operational
--Changes:]]]
--* modified include for Bitbucket repository
--------------------------------------------------------------------------------
--Version 4.1.0.64.5
--Author: C A Newbould
--Date: 2020.01.07
--Status: incomplete; operational
--Changes:]]]
--* ##showInBox## references changed to use of ##write##
--* added //header// value
--------------------------------------------------------------------------------
--Version 4.1.0.64.4
--Author: C A Newbould
--Date: 2020.01.03
--Status: incomplete; operational
--Changes:]]]
--* ##showInBox## tested, using default
--------------------------------------------------------------------------------
--Version 4.1.0.64.3
--Author: C A Newbould
--Date: 2019.11.29
--Status: incomplete; operational
--Changes:]]]
--* ##main## renamed
--* ##main## utilised in a different way
--------------------------------------------------------------------------------
--Version 4.1.0.64.2
--Author: C A Newbould
--Date: 2019.11.11
--Status: incomplete; operational
--Changes:]]]
--* ##show## reversions tested 
--------------------------------------------------------------------------------
--Version 4.1.0.64.1
--Author: C A Newbould
--Date: 2019.11.10
--Status: incomplete; operational
--Changes:]]]
--* ##show## modifications tested 
--------------------------------------------------------------------------------
--Version 4.1.0.64.0
--Author: C A Newbould
--Date: 2019.11.09
--Status: incomplete; operational
--Changes:]]]
--* created
--* ##show## tested
--------------------------------------------------------------------------------

