/*
=Object-Centred Euphoria: test bed for capturing and separating STDOUT from STDERR

*Version: 4.1.0.32.3
*Author: C A Newbould
*Date: 2020.08.25
*Status: complete; operational
*Changes:
** corrected errors in constants

==DETAIL

The output is largely self-explanatory.

The application exploits the error-management of OCE to overcome the
limitations of Euphoria's re-direction facility in MS Windows.

===The code
*/

include object.e -- to access full range of OCE functionality

procedure main(sequence heading)
    write(repeat('-', lengthOf(heading)), LF)
    write(heading, LF)
    write(repeat('-', lengthOf(heading)), LF)
    write("This little app tests the separation of output & error streams ")
    write("as devised by Euphoria's Object-Centred system.", LF)
    write()
    write("First, we use the standard direction: STDOUT sent immediately to ")
    write("the terminal.", LF)
    write("The STDERR stream is not used, but errors accumulated internally.", LF)
    write()
    write("**FIRST RUN**", LF)
    write()
    write("A message to STDOUT", LF)
    write("A message to STDERR", TOERROR)
    write("** Reading error string now**", LF)
    write(getOutput(FROMERROR), LF)
    write("This now tests doing exactly the same thing in reverse.", LF)
    write("Here, errors are show immediately; ")
    write("with the standard output afterwards.", LF)
    write()
    write("**SECOND RUN**", LF)
    write("A message to STDOUT", TOOUTPUT)
    write("A message to STDERR", LF)
    write("** Reading the output now**", LF)
    write(getOutput(FROMOUTPUT), LF)
    write("*** Enter any key to close app *** ", WAIT)
end procedure

/*


==  EXECUTION

*/

main("Testing separation of output streams")

/*

== PREVIOUS VERSIONS

*Version: 4.1.0.32.2
*Author: C A Newbould
*Date: 2020.05.15
*Status: complete; operational
*Changes:
** edited to use ##getOutput##

*Version: 4.1.0.32.1
*Author: C A Newbould
*Date: 2020.04.30
*Status: complete; operational
*Changes:
** edited to "literal programming" layout

*Version: 4.1.0.32.0
*Author: C A Newbould
*Date: 2020.04.15
*Status: complete; operational
*Changes:
** created

*/
