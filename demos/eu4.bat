@echo off
::Version 4.1.0.1; Author C A Newbould; Date 2020.05.04 - add path
::Version 4.1.0.0; Author C A Newbould; Date 2020.04.29
ECHO Running OCE examples on OE4.1.0(64-bit)

set EUDIR=\euphoria64
set path=%path%;%EUDIR%\dlls;

%EUDIR%\bin\eui %1
