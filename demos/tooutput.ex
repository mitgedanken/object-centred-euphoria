/*
=Object-Centred Euphoria: test bed for immediate error reporting

*Version: 4.1.0.32.1
*Author: C A Newbould
*Date: 2020.05.15
*Status: complete; operational
*Changes:
** modified to call ##getOutput##
==DETAIL

The output is largely self-explanatory.

The application exploits the options within the ##write## routine of OCE
to overcome the
limitations of Euphoria's re-direction facility in MS Windows.

*/

include object.e -- to access full range of OCE functionality

constant EOL = '\n'

procedure main(sequence heading)
    write(repeat('-', lengthOf(heading)) & EOL, TOOUTPUT)
    write(heading & EOL, TOOUTPUT)
    write(repeat('-', lengthOf(heading)) & EOL, TOOUTPUT)
    write("This little app tests the separation of output & error streams ")
    write("as devised by Euphoria's Object-Centred system.", LF)
    write()
    write("First, we use the standard direction: material sent immediately to ")
    write("the terminal.", LF)
    write("The results are accumulated internally.", LF)
    write()
    write("**THE RUN**", LF)
    write()
    write("This is a message that is part of the results" & EOL, TOOUTPUT)
    write("It is now time to look at the output", LF)
    write("** Displaying the Output now **", LF)
    string output_string = getOutput()
    if lengthOf(output_string) then
        write(output_string, LF)
    end if
    write("*** END OF OUTPUT ***", LF)
    write("*** Enter any key to close app *** ", WAIT)
end procedure

/*


==  EXECUTION

*/

main("Testing separation of output streams")

/*

== PREVIOUS VERSIONS

*Version: 4.1.0.32.0
*Author: C A Newbould
*Date: 2020.05.14
*Status: complete; operational
*Changes:
** revised from //errors.ex//

*Version: 4.1.0.32.0
*Author: C A Newbould
*Date: 2020.04.15
*Status: complete; operational
*Changes:
** created

*/
