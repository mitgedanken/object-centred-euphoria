--html.ex

-- Testbed for the TOHTML option of 'write' in output.e

-- Output sent to both HEAD and BODY

-- Javascript call made

-- Output collected, read and displayed

-- Note that writeTo needed to complete this properly

-- Version: 4.1.0.2
-- Author: C A Newbould
-- Date: 2020.12.31
-- Status: operational; complete
-- Changes:
--* added alternative for non-MS OS

include ../include/object.e

constant SCRIPT = "script"

-- Enter a title
write("Euphoria Test Output", TOHTML, TITLE, TOHEAD)
-- write a few lines of body
write("This is my main heading", TOHTML, H1)
write("This is my first paragraph", TOHTML, P)
write("This is just a bit of text", TOHTML)
write("This is emboldened text", TOHTML, B)
write("This is just another bit of text", TOHTML)
write("This is text with a line-break <br> in it", TOHTML)
write("<br>This is text in italics", TOHTML, I)
write("This is Heading 5 text", TOHTML, H5)
write("Here is some analytical output from Euphoria:", TOHTML, H3)
-- calculation
integer x = 12, y = 19, z = x + y
write(sprintf("The sum of %d and %d is %d", {x, y, z}) & BR, TOHTML, I)
write("The product is ", TOHTML)
write(sprintf("%d", x * y) & BR, TOHTML, B)
write("window.alert('Euphoria finished - close me to see the output');", TOHTML, SCRIPT)
write("THE END", TOHTML, P)
--write("!!! TOHTML complete !!!", WAIT)
-- Get the output
string htmlout = getOutput(FROMHTML)
-- Write the html to a file
filehandle fh = open("test.html", , TOWRITE)
writeTo(fh, htmlout)
close(fh) -- needed before the file can be opened in a browser
-- Output it
ifdef WINDOWS then
    system("start test.html", 0)
elsedef
    system("firefox test.html", 0)
end ifdef

-- Version: 4.1.0.1
-- Author: C A Newbould
-- Date: 2020.05.18
-- Status: operational; complete
-- Changes:
--* added test for ##writeTo##
