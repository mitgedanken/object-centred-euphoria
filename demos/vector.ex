--------------------------------------------------------------------------------
--  vector.ex
--------------------------------------------------------------------------------
-- Test bed for routines defined for the vector type.
--
-- Tested on the following:
-- MS Windows
--  OE4.1.0(64)
--  OE4.1.0(32)
--  OE4.0.5
-- Linux
--  OE4.1.0(64)
--------------------------------------------------------------------------------
--Version 4.1.0.64.9
--Author: C A Newbould
--Date: 2020.02.03
--Status: incomplete; operational
--Changes:]]]
--* modified include for Bitbucket repository
--------------------------------------------------------------------------------
include object.e
--------------------------------------------------------------------------------
constant PROMPT = "The sequence is ", LENGTH = "Its length is %d\n",
            VECTOR = "Is it a vector? ", YES = "Yes", NO = "No",
            DOTS = "......", SUM = ", and the sum is %g\n",
            MEAN = "This gives means of: ", SEMI_COLON = "; "
--------------------------------------------------------------------------------
procedure test(sequence this)   --v4.1.0.64.6
    write(PROMPT)
    show(this)
    show(lengthOf(this), LENGTH)
    write(VECTOR)
    if vector(this) then
        write(YES)
        show(sumOf(this), SUM)
        write(MEAN & convert(meanOf(this), TOSTRING) & SEMI_COLON   --v4.1.0.64.8
                    & convert(meanOf(this, GEOM), TOSTRING) & SEMI_COLON   --v4.1.0.64.8
                    & convert(meanOf(this, HARM), TOSTRING), LF)   --v4.1.0.64.8
    else
        write(NO, LF)
    end if
    write(DOTS, LF)
end procedure
--------------------------------------------------------------------------------
procedure main(sequence heading) --v4.1.0.64.6
    write(repeat('-', lengthOf(heading)), LF) --Using Core routine v4.1.0.64.6
    write(heading, LF) --v4.1.0.64.6
    write(repeat('-', lengthOf(heading)), LF) --Using Core routine v4.1.0.64.6
    test(Vector(1,,,5))   --{1,2,3,4,5})   --v4.1.0.64.6
    test("Hello" & {1.111,2.222})   --v4.1.0.64.6
    test(addTo("Hello", {1.111,2.222}))   --v4.1.0.64.6
    write("*** Enter any key to close app *** ", WAIT) --v4.1.0.64.6
end procedure   --v4.1.0.64.6
--------------------------------------------------------------------------------
--  Execution
--------------------------------------------------------------------------------
main("Testing vector routines") --v4.1.0.64.6
--------------------------------------------------------------------------------
--  Previous versions
--------------------------------------------------------------------------------
--Version 4.1.0.64.8
--Author: C A Newbould
--Date: 2019.12.30
--Status: incomplete; operational
--Changes:]]]
--* modified to use ##convert##
--------------------------------------------------------------------------------
--Version 4.1.0.64.7
--Author: C A Newbould
--Date: 2019.11.29
--Status: incomplete; operational
--Changes:]]]
--* modified include
--------------------------------------------------------------------------------
--Version 4.1.0.64.6
--Author: C A Newbould
--Date: 2019.11.29
--Status: incomplete; operational
--Changes:]]]
--* ##test## renamed
--* ##test## utilised in a different way
--------------------------------------------------------------------------------
--Version 4.1.0.64.5
--Author: C A Newbould
--Date: 2019.11.28
--Status: incomplete; operational
--Changes:]]]
--* modified to test ##Vector##
--------------------------------------------------------------------------------
--Version 4.1.0.64.4
--Author: C A Newbould
--Date: 2019.11.13
--Status: incomplete; operational
--Changes:]]]
--* modified to use ##formatToScreen##
--* modified to use ##toString##
--------------------------------------------------------------------------------
--Version 4.1.0.64.4
--Author: C A Newbould
--Date: 2019.11.13
--Status: incomplete; operational
--Changes:]]]
--* modified to use ##formatToScreen##
--* modified to use ##toString##
--------------------------------------------------------------------------------
--Version 4.1.0.64.3
--Author: C A Newbould
--Date: 2019.11.12
--Status: incomplete; operational
--Changes:]]]
--* added test of ##mean##
--* modified to use ##addTo##
--------------------------------------------------------------------------------
--Version 4.1.0.64.2
--Author: C A Newbould
--Date: 2019.11.11
--Status: incomplete; operational
--Changes:]]]
--* added test of ##sum##
--------------------------------------------------------------------------------
--Version 4.1.0.64.1
--Author: C A Newbould
--Date: 2019.11.11
--Status: incomplete; operational
--Changes:]]]
--* modified to include call to ##appendTo##
--------------------------------------------------------------------------------
--Version 4.1.0.64.0
--Author: C A Newbould
--Date: 2019.11.11
--Status: incomplete; operational
--Changes:]]]
--* created
--* ##vector## tested 
--* ##lengthOf## tested
--------------------------------------------------------------------------------
