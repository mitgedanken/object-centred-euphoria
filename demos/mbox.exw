--------------------------------------------------------------------------------
-- Module: mbox.exw
--------------------------------------------------------------------------------
-- Description: demonstration of using OCE applied to the WIN API to create a message-box function
-- which is then invoked, capturing the first response but not the second
-- Location: Bitbucket>object-centred-euphoria>demos>
-- Version: 4.0.5.0
-- Author: C A Newbould
-- Date: 2021.01.14
-- Status: complete
-- Changes:
--* created
--------------------------------------------------------------------------------
-- Includes
--------------------------------------------------------------------------------
include object.e -- always include this sub-library for OCE apps
--------------------------------------------------------------------------------
-- Constants:
--------------------------------------------------------------------------------
-- Define MB constants only for this example
constant MB_OK = #00
constant MB_OKCANCEL = #01
--------
-- Define all ID values - response codes
public enum IDOK, IDCANCEL, IDABORT, IDRETRY, IDIGNORE, IDYES, IDNO
--------------------------------------------------------------------------------
-- Routines:
--------------------------------------------------------------------------------
function MessageBox(string title, string text, object style)    -->  [integer] user response
    clib user32 = open("user32.dll", ASDLL)
    crid msgbox = define(user32, "MessageBoxA", {C_POINTER, C_POINTER, C_POINTER, C_INT}, C_INT)
    crid get_active_window = define(user32, "GetActiveWindow", {}, C_LONG)
    if atom(style) then -- style = style
    else style = allOred(style) -- vector
    end if
    return execC(msgbox, {execC(get_active_window, {}), convert(text, TOC), convert(title, TOC), style})
end function    -- MessageBox
--------------------------------------------------------------------------------
procedure main(string title)
    string text = "I am a message box"
    integer my_message = MessageBox(title, text, MB_OKCANCEL)
    if my_message = IDOK then
        text = "You pressed OK"
        MessageBox(title, text, MB_OK) -- not trapping return value
    elsif my_message = IDCANCEL then
        text = "You pressed Cancel"
        MessageBox(title, text, MB_OK) -- not trapping return value
    end if 
end procedure
--------------------------------------------------------------------------------
-- Execution
--------------------------------------------------------------------------------
main("Testing MessageBox Class")
--------------------------------------------------------------------------------
-- Previous Versions
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
