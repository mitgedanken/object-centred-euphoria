--------------------------------------------------------------------------------
--	Library: object.e
--------------------------------------------------------------------------------
-- Notes:
--
-- The version system is OEversion.running series, starting at 0
--------------------------------------------------------------------------------
--/*
--= Library: (euphoria410_64)(include)(oce)object.e
-- Description: The core library module for Object-Centred Euphoria.
------
--[[[Version: 4.1.0.24
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2021.01.15
--Status: incomplete; operational
--Changes:]]]
--* //public// changed to //global//
--
--==Object-Centred Euphoria library: object
--
-- This library hold values to support the OCE ##object## library.
--
-- This is the essential extension module for OCE and must be included
-- directly, or indirectly, in all OCE applications and any user library
-- modules.
--
-- In devising OCE quite a bit of consideration has been put into the
-- approaches to reporting results. A deficiency in Euphoria is its inability
-- to separate the standard error and standard output streams. Whilst it is
-- possible to re-direct standard output on the command line, no such option
-- is available for error reporting. As a consequence error messages are typically
-- mixed up with the standard output results - not a very happy or elegant
-- arrangement.
--
-- As a consequence OCE adopts a different strategy. OCE offers two output
-- routines, ##show## and ##write##. The former is relatively rudimetary, despite
-- the ability to "package" the value though formatting. It always sends
-- details of an object directly to standard output.
-- There is no option to write specifically to the standard error stream.
--
-- On the other hand, the ##write## procedure offers a range of options for
-- separating these different forms of output. The key principle is that, in most
-- cases, the programmer wants immediate "error" feedback to the terminal/console,
-- whereas the output (results of various processes within the application)
-- is be looked at "at leisure", perhaps in a more pleasing format than a
-- terminal window can offer.
--
-- Consequently ##write## offers a range of strategies for directing the
-- information about an object to alternative outputs. These are outlined here:
--
-- * use the //TOERROR// option to store information which is not part of the
--   main output (most likely used once a process is tested and errors are rare)
-- * use the //TOOUTPUT// option to collect together the application's main
--   output, for revealing after all internal processes are complete
--   (used when it is desired that the error reporting is immediate but
--    the results are only needed "at the end" of a successful run)
-- * use the //TOABOX// option to issue immediate information to the user, in
--   the form of a pop-up message dialog
-- * use the //TOHTML// option to collect together material intended for the
--   application's main output, using html tags to aid the formatting of the
--   material
--
-- A routine ##getOutput## exists to access the accumulated material from
-- //TOOUTPUT// and/or //TOERROR//, using argumets of //OUTPUT// (the default)
-- or //ERROR//, respectively, to "draw down" these stores.
--
--=== What the module contains
-- The following are defined in this module, which acts as the fundamental OCE
-- extension to Core Euphoria.
--
-- Note that this module also incorporates those values and routines declared
-- as //public// in the two modules //atom.e// and //sequence.e//.
--
-- In addition this module contains that functionality which requires 
-- the combination of facilities in both //atom.e// and //sequence.e//.
--
--===Constants
--* ##ABORTED##
--* ##CANCELLED##
--* ##FALSE##
--* ##IGNORED##
--* ##NOED##
--* ##OK##
--* ##OKCANCEL##
--* ##OKED##
--* ##RETRIED##
--* ##TRUE##
--* ##YESED##
--===Variables
--* string ##error_string##
--===Types (and their routines)
--* **object**(built-in)
--** ##convert##(object, integer)	:	object
--** ##lengthOf##(object)  :   integer
--** ##show##(object[, string])
--* additional routines for **string** values
--** ##getOutput(string) : string
--** ##write##([string][, integer])	:	integer
--* additional routines for **filehandle** values
--** ##writeTo##(filehandle, object) :	boolean
--
-- Utilise this support by adding the following statement to your module:
--<eucode>include oce/object.e</eucode>
------
--*/
--------------------------------------------------------------------------------
--/*
--==Interface
--*/
--------------------------------------------------------------------------------
--/*
--=== Includes
--*/
--------------------------------------------------------------------------------
include atom.e
include sequence.e
--------------------------------------------------------------------------------
--/*
--=== Constants
--*/
--------------------------------------------------------------------------------
--	Local
--------------------------------------------------------------------------------
constant C_ANGLE = ">"
constant CLOSE = "/"
constant EMPTY_SEQUENCE = {}
constant END = TRUE
constant EOL = "\n"
constant M_ALLOC = 16
constant M_WAIT_KEY = 26 
constant O_ANGLE = "<"
constant SCREEN = 1
constant VDIGITS = "0123456789ABCDEFabcdef"
--------------------------------------------------------------------------------
--	Shared with other modules
--------------------------------------------------------------------------------
global enum CTOSTRING, TOC, TONUMBER, TOSTRING	-- for convert
global enum NOTHING = 0, LF, WAIT, TOERROR, TOABOX, TOOUTPUT, TOHTML	-- for write
ifdef WINDOWS then
    export constant OK = #00
    export constant OKCANCEL = #01
    export enum OKED, CANCELLED, ABORTED, RETRIED, IGNORED, YESED, NOED
elsifdef LINUX then
    export constant OK = 1  -- dummy for now (need proper value)
    export constant OKCANCEL = 2  -- dummy for now (need proper value)
    export enum OKED	-- 4.1.0.64.18, for completeness
--elsedef
--
end ifdef
global constant FROMERROR = "error_" -- for getOuput
global constant FROMOUTPUT = "output_" -- for getOuput
global constant FROMHTML = "html_" -- for getOuput
global enum TOBODY = 0, TOHEAD  -- for TOHTML sections
-- TOHTML tags
global constant B = "b"
global constant BR = "<br>"
global constant H1 = "h1"
global constant H2 = "h2"
global constant H3 = "h3"
global constant H4 = "h4"
global constant H5 = "h5"
global constant I = "i"
global constant P = "p"
global constant HTML = "html"
global constant BODY = "body"
global constant HEAD = "head"
global constant TITLE = "title"
--------------------------------------------------------------------------------
--
--=== Euphoria types
--
--------------------------------------------------------------------------------
--	Local
--------------------------------------------------------------------------------
--	Shared with other modules
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--/*
--=== Variables
--*/
--------------------------------------------------------------------------------
--	Local
--------------------------------------------------------------------------------
--	Shared with other modules
--------------------------------------------------------------------------------
string error_string = EMPTY_SEQUENCE	-- for error messages
string output_string = EMPTY_SEQUENCE -- for storing output
string body_string = EMPTY_SEQUENCE    -- for HTML body
string head_string = EMPTY_SEQUENCE    -- for HTML head
--------------------------------------------------------------------------------
--/*
--=== Routines
--*/
--------------------------------------------------------------------------------
--	Local
--------------------------------------------------------------------------------
function angle(sequence code, boolean ending = FALSE) -- surrounds [\]code with angle brackets
    return O_ANGLE & iif(ending, CLOSE, EMPTY_SEQUENCE) & code & C_ANGLE
end function
--------------------------------------------------------------------------------
function embed(sequence this, sequence code) -- embeds the text inside an angle-bracketed-code segment
    return angle(code) & this & angle(code, END)
end function
--------------------------------------------------------------------------------
function showInBox(string message, string caption, object buttons=OK)    -- [integer] displays the message in a GUI box
    ifdef WINDOWS then    
        -- open the relevant file
        clib source = open("user32.dll", ASDLL)
        crid messagebox = define(source, "MessageBoxA", 
                    {C_POINTER, C_POINTER, C_POINTER, C_UINT}, C_INT)
        crid getactivewindow = define(source, "GetActiveWindow", {}, C_POINTER)
        -- need to check for multiple flags
        integer style = 0
        if atom(buttons) then
            style = buttons
        else
            for i = 1 to length(buttons) do
                style = or_bits(style, buttons[i])    -- Uses Core routine
            end for    
        end if
        return execC(messagebox, {execC(getactivewindow),
                    convert(message, TOC), convert(caption, TOC), style
                    })
    elsifdef LINUX then
        -- use an installed command
        system("dialog --title '" & caption & "' --msgbox '" & message & "' 0 0")
        return 1
    elsedef
        return -1   -- temporary until Linux done
    end ifdef
end function
--------------------------------------------------------------------------------
function toC(string this)
	atom mem = machine_func(M_ALLOC, lengthOf(this) + 1)	--use Core routine -- Thanks to Igor
	if mem then	-- allocation successful
		poke(mem, this)	--use Core routine
		poke(mem + lengthOf(this), 0)	--use Core routine  -- Thanks to Aku
	end if
	return mem
end function
--------------------------------------------------------------------------------
function to_number(string text_in, integer return_bad_pos = 0)
	-- get the numeric result of text_in
	integer decimal_mark = '.'
	integer lDotFound = 0
	integer lSignFound = 2
	integer lCharValue
	integer lBadPos = 0
	atom    lLeftSize = 0
	atom    lRightSize = 1
	atom    lLeftValue = 0
	atom    lRightValue = 0
	integer lBase = 10
	integer lPercent = 1
	atom    lResult
	integer lDigitCount = 0
	integer lCurrencyFound = 0
	integer lLastDigit = 0
	integer lChar
	for i = 1 to length(text_in) do
		if not integer(text_in[i]) then
			exit
		end if
		lChar = text_in[i]
		switch lChar do
			case '-' then
				if lSignFound = 2 then
					lSignFound = -1
					lLastDigit = lDigitCount
				else
					lBadPos = i
				end if
			case '+' then
				if lSignFound = 2 then
					lSignFound = 1
					lLastDigit = lDigitCount
				else
					lBadPos = i
				end if
			case '#' then
				if lDigitCount = 0 and lBase = 10 then
					lBase = 16
				else
					lBadPos = i
				end if
			case '@' then
				if lDigitCount = 0  and lBase = 10 then
					lBase = 8
				else
					lBadPos = i
				end if
			case '!' then
				if lDigitCount = 0  and lBase = 10 then
					lBase = 2
				else
					lBadPos = i
				end if
/*
			case '$', '�', '�', '�', '�' then
				if lCurrencyFound = 0 then
					lCurrencyFound = 1
					lLastDigit = lDigitCount
				else
					lBadPos = i
				end if
*/
			case '_' then -- grouping character
				if lDigitCount = 0 or lLastDigit != 0 then
					lBadPos = i
				end if
			case '.', ',' then
				if lLastDigit = 0 then
					if decimal_mark = lChar then
						if lDotFound = 0 then
							lDotFound = 1
						else
							lBadPos = i
						end if
					else
						-- Ignore it
					end if
				else
					lBadPos = i
				end if
			case '%' then
				lLastDigit = lDigitCount
				if lPercent = 1 then
					lPercent = 100
				else
					if text_in[i-1] = '%' then
						lPercent *= 10 -- Yes ten not one hundred.
					else
						lBadPos = i
					end if
				end if
			case '\t', ' ', #A0 then
				if lDigitCount = 0 then
					-- skip it
				else
					lLastDigit = i
				end if
			case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
			      'A', 'B', 'C', 'D', 'E', 'F',
			      'a', 'b', 'c', 'd', 'e', 'f' then
	            lCharValue = find(lChar, VDIGITS) - 1
	            if lCharValue > 15 then
	            	lCharValue -= 6
	            end if
	            if lCharValue >= lBase then
	                lBadPos = i
	            elsif lLastDigit != 0 then  -- shouldn't be any more digits
					lBadPos = i
				elsif lDotFound = 1 then
					lRightSize *= lBase
					lRightValue = (lRightValue * lBase) + lCharValue
					lDigitCount += 1
				else
					lLeftSize += 1
					lLeftValue = (lLeftValue * lBase) + lCharValue
					lDigitCount += 1
				end if
			case else
				lBadPos = i
		end switch
		if lBadPos != 0 then
			exit
		end if
	end for
	-- Error if no actual digits where converted.
	if lBadPos = 0 and lDigitCount = 0 then
		lBadPos = 1
	end if
	if return_bad_pos = 0 and lBadPos != 0 then
		return 0
	end if
	if lRightValue = 0 then
		-- Common situation optimised for speed.
	    if lPercent != 1 then
			lResult = (lLeftValue / lPercent)
		else
	        lResult = lLeftValue
		end if
	else
	    if lPercent != 1 then
	        lResult = (lLeftValue  + (lRightValue / (lRightSize))) / lPercent
	    else
	        lResult = lLeftValue + (lRightValue / lRightSize)
	    end if
	end if
	if lSignFound < 0 then
		lResult = -lResult
	end if
	if return_bad_pos = 0 then
		return lResult
	end if
	if return_bad_pos = -1 then
		if lBadPos = 0 then
			return lResult
		else
			return {lBadPos}	
		end if
	end if
	return {lResult, lBadPos}
end function
--------------------------------------------------------------------------------
function toString(sequence this)    -- converts the sequence into a string text
	sequence s = "{"
	for i = 1 to lengthOf(this) do
		if atom(this[i]) then
			s &= sprintf("%.10g", this[i])	--use Core routine
		else
			s &= toString(this[i])
		end if
		s &= ','
	end for
	if s[$] = ',' then
		s[$] = '}'
	else
		s &= '}'
	end if
	return s
end function
--------------------------------------------------------------------------------
--
-- Parameter:
--# ##this##: any Euphoria non-string sequence
--
-- Returns:
--
-- a **sequence**: a string representation of ##this##.
--------------------------------------------------------------------------------
--	Shared with other modules
--------------------------------------------------------------------------------
global function convert(object this, integer toType=TOSTRING) -- : object - converts the source to a different type
	if atom(this) then
		switch toType do
			case CTOSTRING then
				return peek_string(this)    -- uses Core routine 
			case TOSTRING then
				return sprintf("%.10g", this)	--use Core routine			
		end switch
	else
		if string(this) then
			switch toType do
				case TOC then
					return toC(this)
				case TONUMBER then
					return to_number(this)
			end switch
		else	-- sequence
			switch toType do
				case TOSTRING then
					return toString(this)
			end switch
		end if
	end if
end function
--------------------------------------------------------------------------------
--/*
--Parameters:
--# ##this##: the object to be converted
--# ##toType##: a signal for the destination type - one of CTOSTRING, TOC,
-- TONUMBER, TOSTRING
--
--Returns:
--
-- an **object**, but with the type indicated by ##toType##
--
-- Values are:
--* if ##address(this)## then ##CTOSTRING## returns as a Euphoria **string**
-- the C-style zero-terminated string stored at ##this##
--* if ##atom(this)## then ##TOSTRING## yields a **string** holding the value
-- as a quasi-decimal
--* if ##string(this)## then ##TOC## yields an **address** pointing to the
-- C-style zero-terminated string holding ##this##
--* if ##string(this)## then ##TONUMBER## converts the number-bearing source
-- to an **atom** containing the converted value
--* if ##sequence(this)## then ##TOSTRING## yields a **string** holding a
-- printable version of the sequence's value
--*/
--------------------------------------------------------------------------------
global function lengthOf(object this)   -- [integer] the length of the object
    return length(this) -- defined using Core routine
end function
--------------------------------------------------------------------------------
--/*
--Parameter:
--# ##this##: the object whose length is to be determined
--
--Returns:
--
--an **integer**: the length of ##this##
--
--Notes:
--
--* The length of an **atom** is always 1.
--* The length of an empty sequence is 0.
--* The length of an embedded empty sequence is 1.
--
--*/
--------------------------------------------------------------------------------
global procedure show(object this, string format=EMPTY_SEQUENCE)  -- displays value to the terminal window [formatted]
    if equal(format, EMPTY_SEQUENCE) then
		?this   -- defined using Core routine
    else
		printf(SCREEN, format, {this})   -- defined using Core routine
    end if
end procedure
--------------------------------------------------------------------------------
--/*
--Parameters:
--# ##this##: the object to be displayed
--# ##format##: the format to embed the object
--
-- Notes:
--
-- The value assigned to ##format##, where not empty, must contain a format
-- specifier appropriate to the type of value stored. Any other text may be
-- added to embellish the display.
--
-- The basic format specifiers are:
--
-- * ##%d## ~-- print an atom as a decimal integer
-- * ##%x## ~-- print an atom as a hexadecimal integer. Negative numbers are printed
--            in two's complement, so -1 will print as ##FFFFFFFF##
-- * ##%o## ~-- print an atom as an octal integer
-- * ##%s## ~-- print a sequence as a string of characters, or print an atom as a single
--            character
-- * ##%e## ~-- print an atom as a floating-point number with exponential notation
-- * ##%f## ~-- print an atom as a floating-point number with a decimal point but no exponent
-- * ##%g## ~-- print an atom as a floating-point number using whichever format seems
--            appropriate, given the magnitude of the number
-- * ##~%~%## ~-- print the ##'%'## character itself. This is not an actual format specifier.
--
-- Field widths can be added to the basic formats (for example: ## %5d, %8.2f, %10.4s##). The number
-- before the decimal point is the minimum field width to be used. The number after
-- the decimal point is the precision to be used for numeric values.
--
-- If the field width is negative (for example ##%-5d##) then the value will be left-justified
-- within the field. Normally it will be right-justified, even strings. If the field width
-- starts with a leading 0 (for example ##%08d##) then leading zeros will be supplied to fill up
-- the field. If the field width starts with a ##'+'## (for example ##%+7d##) then a plus sign will
-- be printed for positive values.
--*/
--------------------------------------------------------------------------------
global function write(string this="\n", integer action=NOTHING, string captionortag=EMPTY_SEQUENCE, object buttonsorout=OK)   -- writes to the terminal window [with a line-feed or a pause] | to error variable | to a message-box
    integer ret = 1
    switch action do
        case TOABOX then
            ret = showInBox(this, captionortag, buttonsorout)
        case TOERROR then -- add this to the store
            error_string &= this & '\n'
        case TOOUTPUT then -- add this to the store
            output_string &= this & '\n'
        case TOHTML then
            sequence line
            if equal(captionortag, EMPTY_SEQUENCE) then
                line = this
            else
                line = embed(this, captionortag)
            end if
            switch buttonsorout do -- add this (or tagged this) to the store
                case TOBODY then
                    body_string &= line & EOL
                case TOHEAD then
                    head_string &= line & EOL
            end switch
        case LF then
            puts(SCREEN, this & EOL)   -- uses Core routine
        case WAIT then
            puts(SCREEN, this)   -- uses Core routine
			machine_func(M_WAIT_KEY, 0)   -- uses machine code routine
		case else
			puts(SCREEN, this)   -- uses Core routine
	end switch
    return ret
end function
--------------------------------------------------------------------------------
--/*
--Parameters:
--# //this//: the string to be displayed [Default "\n"]
--# //action//: the action to be taken [Default NOTHING]
--# //captionottag//: the caption to use if //TOABOX// is chosen [Default ""] or
--  the html tag if //TOHTML// chosen
--# //buttonsorout//: the code(s) signifying the button[s] to include in the message-box,
-- if //TOABOX// is chosen [Default OK] or
-- the section of the html output (//TOBODY// or //TOHEAD//) if //TOHTML// is chosen
--
-- This is one of:
-- //NOTHING// [Default]; //LF//; //TOABOX//; //TOERROR//; //TOOUTPUT//; //TOHTML// or //WAIT//.
--
-- If //NOTHING//, //LF// or //WAIT// is chosen then the string is sent to the
-- standard output device (the Screen).
-- If //LF// is chosen then a line-feed follows; 
-- if //WAIT// is chosen then the terminal window stays open
-- until a key is entered.
--
-- The //TOERROR// option adds the string to a dedicated error store
-- (a **string** variable), which the user can utilise to report (cumulative)
-- errors before exiting an application.
--
-- The //TOOUTPUT// option adds the string to a dedicated output store
-- (a **string** variable), which the user can utilise to report results
-- before exiting an application.
--
-- The //TOHTML// option adds the string, suitably tagged, to either the body
-- or the head of dedicated html stores (**string** variables) reserved for
-- building html output. The tag (one of the pre-coded ones - see above -
-- or one incorporated into the call, eg "code") is passed as the third
-- parameter, with the fourth signifying destination the destination
-- (//TOBODY// [Default] or //TOHEAD//). 
--
-- The //TOABOX// option sends the string to a message-box window,
-- which is displayed with the caption as a title and with one or more buttons,
-- determined by the //buttons// code.
--
-- Returns:
--
-- an **integer**: //1// for all options bar //TOABOX//, where it is the exit
-- signal returned when the user closes the message-box. 
--
-- Notes:
--
-- The pre-defined options for //buttons// are all **integer**s and should
-- normally suffice. If, however, a combination is required then //buttons//
-- should be a **vector** containing the several choices.
--
-- Calling ##write##() produces a "writeln()" effect: a line-feed.
--*/
--------------------------------------------------------------------------------
global function getOutput(string typ = FROMOUTPUT)	-- returns the ERROR|OUTPUT string
	if equal(typ, FROMERROR) then
		return error_string
	elsif equal(typ, FROMHTML) then
        sequence b = embed(body_string, BODY)
        sequence h = embed(head_string, HEAD)
        sequence result = h & b
        return embed(result, HTML)
    else
		return output_string
	end if
end function
--------------------------------------------------------------------------------
--/*
-- Parameter:
--# //typ//: the type of output sought - ERROR|OUTPUT (default)
--
-- Returns:
--
-- a **string** containing the accumulated text "redirected" to the respective internal
-- stores
--*/
--------------------------------------------------------------------------------
global function writeTo(filehandle this, object val=EOL) -- writes a value to a file's open stream
    if this > 2 then --OK
        if atom(val) then -- a BYTE
            puts(this, val)
        elsif string(val) then -- a LINE
            puts(this, val)
        elsif vector(val) then -- BYTES (cf write_file)
            for i = 1 to lengthOf(val) do
                puts(this, val[i])
            end for
        else -- assumed to be a sequence of strings - ASLINES (cf write_lines)
            string output = EMPTY_SEQUENCE
            for i = 1 to lengthOf(val) do
                if string(val[i]) then -- OK
                    output &= val[i] & EOL
                else
                    return FALSE
                end if
            end for
            -- if reached here then accumulated output needs writing
            puts(this, output)
        end if
    else -- not a valid and active filehandle
        return FALSE
    end if
    return TRUE
end function
--------------------------------------------------------------------------------
--/*
-- Parameters:
--# //this//: the filehandle, which must have been opened previously and be
--  valid for writing
--# //val//: the value to be sent to the file [Default EOL]
--
-- Returns:
--   a **boolean**: TRUE if processing is successfully effected
--
-- The action taken depends solely upon the value to be written:
--* if an **atom** then the low order 8-bits of //val// are written
--* if a **string** then //val// alone is ouput; so if you want //EOL// as well
--  then you must add it to //val// before calling ##writeTo##, or make
--  //writeTo(<this>)// the next output line [cf. write()]
--* if a **vector** then the stream of atoms is written sequentially with no
--  separators
--* if a **sequence** then the sequence is assumed to be an array of **string**s.
--  This is tested for; only if this is verified will the
--  output be written - as a stream of "lines" - each automatically
--  terminated with //EOL//.
--
-- Errors:
--
-- The return value is /FALSE// if either:
--* //this// does not refer to a valid and active filehandle, with write access
--* //val// contains non-string characters
--* //val// has embedded sub-sequences
--
--*/
--------------------------------------------------------------------------------
--
--==== Defined instances
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Previous versions
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.23
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.08.03
--Status: incomplete; operational
--Changes:]]]
--* ##write## omission sorted
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.22
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.05.18
--Status: incomplete; operational
--Changes:]]]
--* ##writeTo## defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.21
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.05.17
--Status: incomplete; operational
--Changes:]]]
--* ##write##: //TOHTML// option added
--* adjusted defaults st //TOHTML// and //TOABOX// synchronised
--* corresponding constants and variables added
--* ##getOutput## extended
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.20
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.05.15
--Status: incomplete; operational
--Changes:]]]
--* ##getOutput## defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.19
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.05.14
--Status: incomplete; operational
--Changes:]]]
--* //TOOUTPUT// defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.18
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.05.03
--Status: incomplete; operational
--Changes:]]]
--* //TOABOX// re-defined for Linux
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.17
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.01.10
--Status: incomplete; operational
--Changes:]]]
--* //TOERROR// in ##write## modified
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.16
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.01.09
--Status: incomplete; operational
--Changes:]]]
--* //export//s changed to //public//
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.15
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.01.09
--Status: incomplete; operational
--Changes:]]]
--* ##boolean## moved tp //atom.e//
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.14
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.01.07
--Status: incomplete; operational
--Changes:]]]
--* ##write## moved here
--* ##write## converted to a function
--* ##showInBox## incorporated into ##write##
--* ##write## further extended with a //TOERROR// option
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.13
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.01.04
--Status: incomplete; operational
--Changes:]]]
--* ##showInBox## revised
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.12
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.01.03
--Status: incomplete; operational
--Changes:]]]
--* general documentation expanded
--* ##showInBox## defined (MS Windows only)
--* ##OK## defined
--* ##OKCANCEL## defined
--* ##OKED##, ##CANCELLED##, ##ABORTED##, ##RETRIED##, ##IGNORED## defined
--* ##YESED##, ##NOED## defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.11
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.01.02
--Status: incomplete; operational
--Changes:]]]
--* ##convert## documentation amended & extended
--* changed argument type in ##show##
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.10
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.12.30
--Status: incomplete; operational
--Changes:]]]
--* ##convert## extended
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.9
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.12.21
--Status: incomplete; operational
--Changes:]]]
--* ##convert## defined
--* ##toString## made local and subsummed within ##convert##
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.8
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.12.21
--Status: incomplete; operational
--Changes:]]]
--* modified to depend on both //atom.e// and //sequence.e//
--* modified all //public// to //export//
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.7
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.29
--Status: incomplete; operational
--Changes:]]]
--* ##show## modified
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.6
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.22
--Status: incomplete; operational
--Changes:]]]
--* ##formatToScreen## removed
--* ##show## consequently extended
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.5
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.13
--Status: incomplete; operational
--Changes:]]]
--* ##toString## defined
--* ##iif## (boolean) defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.4
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.12
--Status: incomplete; operational
--Changes:]]]
--* ##formatToScreen## defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.3
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.11
--Status: incomplete; operational
--Changes:]]]
--* ##show## returned to former state
--* ##sequence-based stuff moved to //sequence.e//
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.2
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.10
--Status: incomplete; operational
--Changes:]]]
--* ##show## modified
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.1
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.09
--Status: incomplete; operational
--Changes:]]]
--* ##show## defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.0
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.08
--Status: incomplete; operational
--Changes:]]]
--* created
--* ##lengthOf## defined
--* ##TRUE## defined
--* ##FALSE## defined
--* **boolean** defined
--------------------------------------------------------------------------------
