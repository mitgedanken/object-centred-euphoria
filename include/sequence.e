--------------------------------------------------------------------------------
--	Library: sequence.e
--------------------------------------------------------------------------------
-- Notes:
--
-- ##partOf##: other selections?
--------------------------------------------------------------------------------
--/*
--= Library: (euphoria410_64)(include)(oce)sequence.e
-- Description: The sequence library module for Object-Centred Euphoria.
------
--[[[Version: 4.1.0.31
--Euphoria Versions: 4.0.5 upwards; built with v4.1.0 [64bit]
--Author: C A Newbould
--Date: 2021.01.15
--Status: incomplete; operational
--Changes:]]]
--* //public// changed to //global// 
--
--==Object-Centred Euphoria library: sequence
--
-- This library hold all the sequence-based functionality in Object-Centred
-- Euphoria.
--
-- This is a key extension module for OCE and must be included
-- directly, or indirectly, in all OCE modules, both library and
-- applications, which need either the **sequence**type or any of its more
-- specialised derivatives.
--
-- This is usually achieved automatically by calling the //object.e// module.
--
-- The following are part of the OCE Sequence Library Extension to Core Euphoria
-- and are visible, either as //public// or //public// - see below for full
-- details.
--
-- Utilise this support by adding the following statement to your module:
--<eucode>include oce/sequence.e</eucode>
-- or, more generally, using:
-- <eucode>include oce/object.e</eucode>
--
--===Constants
--* ##AFTER##, ##BEFORE##
--* ##ARITH##, ##GEOM##, ##HARM##
--* ##HEAD##, ##TAIL##, ##ENDS##
--* ##NOTHING##. ##LF##, ##WAIT##
--* ##TOLOWER##, ##TOUPPER##, ##TOPROPER##
--===Types (and their routines)
--* **filename** (**string**)
--** ##extensionOf##(filename)  :   string
--** ##open##(filename[, threeway][, integer])	:	atom
--* **sequence** (built-in)
--** ##addTo##(sequence, object[, integer])  :   sequence
--** ##findIn##(sequence, object[, integer])  :   integer 
--** ##partOf##(sequence[, integer][, integer])	:	sequence
--** ##remove##(sequence[, integer][, integer])	:	sequence
--** ##removeFrom##(sequence, object[, integer])	:	sequence
--** ##reverse##(sequence[, integer[, integer]])  :   sequence
--* **string**(**sequence**)
--** ##String##([string][, integer])	:	string
--** ##changeCaseOf##(string, integer)	:	string
--** ##matchIn##(string, string[, integer]) : object
--** ##trim##(string[, threeway][, object])	:	string
--* **vector**(**sequence**)
--** ##Vector##(object, object[, integer][,integer]	:	vector
--** ##allOred##(vector)	:	atom
--** ##meanOf##(vector[, threeway])	:	atom
--** ##productOf##(vector)	:	atom
--** ##sumOf##(vector) :   atom
--===Defined Instances
--* ##EMPTY_SEQUENCE##
--
------
--*/
--------------------------------------------------------------------------------
without warning	-- to suppress override warning
--------------------------------------------------------------------------------
--/*
--==Interface
--*/
--------------------------------------------------------------------------------
--
--=== Includes
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--/*
--=== Constants
--*/
--------------------------------------------------------------------------------
--	Local
--------------------------------------------------------------------------------
constant EOS = -1
constant EOL = '\n'
constant FALSE = (1=0)
constant KEYBOARD = 0
constant LOWERS = {}
constant M_OPEN_DLL = 50
constant NOTOPEN = -1
constant SCREEN = 1
constant TO_LOWER = 'a' - 'A'
constant TRUE = (1=1)
--------------------------------------------------------------------------------
--	Shared with other modules
--------------------------------------------------------------------------------
global enum AFTER = 0, BEFORE	-- for addTo
global enum ARITH, GEOM, HARM	-- for mean
global enum ASBINARY, ASDLL, ASTEXT	-- for open
global enum HEAD = 0, TAIL, ENDS	-- for partOf & trim
global enum TOLOWER, TOPROPER, TOUPPER	-- for changeCaseOf
global constant ALL = 0	-- for findIn
global constant END_OF_STREAM = {EOS}
global constant TOAPPEND = 'a'
global constant TOREAD = 'r'
global constant TOWRITE = 'w'
global constant TOUPDATE = 'u'
--------------------------------------------------------------------------------
--/*
--=== Euphoria types
--*/
--------------------------------------------------------------------------------
--	Local
--------------------------------------------------------------------------------
type character(integer this)
    return isCharacter(this)
end type
--------------------------------------------------------------------------------
type filehandle(integer this)
	return this = NOTOPEN or this > 2
end type
--------------------------------------------------------------------------------
type threeway(integer this)
	return this >= 0 and this < 4
end type
--------------------------------------------------------------------------------
--	Shared with other modules
--------------------------------------------------------------------------------
global type filename(sequence this)	-- a text sequence containing only valid file name characters
	return allTheSame(this, "isFilename")
end type
--------------------------------------------------------------------------------
global type string(sequence this)	-- a sequence with only characters
    return allTheSame(this, "isCharacter")
end type
--------------------------------------------------------------------------------
global type vector(sequence this)	-- a sequence with only atoms
    return allTheSame(this, "isAtom")
end type
--------------------------------------------------------------------------------
--
--=== Variables
--
--------------------------------------------------------------------------------
--	Local
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--	Shared with other modules
--------------------------------------------------------------------------------
--/*
--=== Routines
--*/
--------------------------------------------------------------------------------
--	Local
--------------------------------------------------------------------------------
function allTheSame(sequence this, sequence rtne, atom rid=routine_id(rtne), integer start=1)
    for i = start to length(this) do
        if not call_func(rid, {this[i]}) then
            return FALSE
        end if
    end for
    return TRUE
end function
--------------------------------------------------------------------------------
function isAtom(object this)
    return atom(this)
end function
--------------------------------------------------------------------------------
function isCharacter(integer this)
    return this = EOS or this = 9 or this = 10 or this = 13
		or (this > 31 and this < 256)
end function
--------------------------------------------------------------------------------
function isDigit(integer this)
	return this > 47 and this < 58
end function
--------------------------------------------------------------------------------
function isFilename(integer this)
	return this > 32 or (this > 45 and this < 59) or isUpper(this)
		    or this = 92 or this = 95 or isLower(this)
end function
--------------------------------------------------------------------------------
function isLower(integer this)
	return this > 96 and this < 123
end function
--------------------------------------------------------------------------------
function isUnderscore(integer this)
	return this = 95
end function
--------------------------------------------------------------------------------
function isUpper(integer this)
	return this > 64 and this < 91
end function
--------------------------------------------------------------------------------
function toLower(object this)
	return this + (this >= 'A' and this <= 'Z') * TO_LOWER
end function
--------------------------------------------------------------------------------
function toProper(sequence this)
	integer inword = 0	-- Initially not in a word
	integer convert = 1	-- Initially convert text
	sequence res = this		-- Work on a copy of the original, in case we need to restore.
	for i = 1 to length(res) do
		if integer(res[i]) then
			if convert then
				-- Check for upper case
				integer pos = isUpper(res[i])
				if pos = 0 then
					-- Not upper, so check for lower case
					pos = isLower(res[i])
					if pos = 0 then
						-- Not lower so check for digits
						-- n.b. digits have no effect on if its in a word or not.
						pos = isDigit(res[i])
						if pos = 0 then
							-- not digit so check for special word chars
							pos = isUnderscore(res[i])
							if pos then
								inword = 1
							else
								inword = 0
							end if
						end if
					else
						if inword = 0 then
							-- start of word, so convert only lower to upper.
							if pos <= 26 then
								res[i] = toUpper(res[i]) -- Convert to uppercase
							end if
							inword = 1	-- now we are in a word
						end if
					end if
				else
					if inword = 1 then
						-- Upper, but as we are in a word convert it to lower.
						res[i] = toLower(res[i]) -- Convert to lowercase
					else
						inword = 1	-- now we are in a word
					end if
				end if
			end if
		else
			-- A non-integer means this is NOT a text sequence, so
			-- only convert sub-sequences.
			if convert then
				-- Restore any values that might have been converted.
				for j = 1 to i-1 do
					if atom(this[j]) then
						res[j] = this[j]
					end if
				end for
				-- Turn conversion off for the rest of this level.
				convert = 0
			end if
			if sequence(res[i]) then
				res[i] = toProper(res[i])	-- recursive conversion
			end if
		end if
	end for
	return res
end function
--------------------------------------------------------------------------------
function toUpper(object this)
	return this - (this >= 'a' and this <= 'z') * TO_LOWER
end function
--------------------------------------------------------------------------------
--	Shared with other modules
--------------------------------------------------------------------------------
--/*
--====The sequence object
--*/
--------------------------------------------------------------------------------
global function addTo(sequence this, object that, integer posn=AFTER)    -- adds an object to the source [AFTER|BEFORE]
    if posn = AFTER then
		return append(this, that)   -- uses Core routine
    else -- posn is positive (BEFORE = 1)
		return splice(this, that, posn)   -- uses Core routine (prepend subsumed)
    end if
end function
--------------------------------------------------------------------------------
--/*
--Parameters:
--# ##this##: the source data
--# ##that##: the additional data
--# ##posn##: the position of the addition - AFTER [Default], BEFORE,
-- or spliced at index ##posn##
--
--Returns:
--
--a **sequence**, comprising the appropriate combination of the 
--two data sources
--
--Notes:
--
--The result's length will be the sum of the two lengths
--*/
--------------------------------------------------------------------------------
global function findIn(sequence this, object that, integer what=1)	-- seeks [all cases of] an object inside a sequence
	integer start = what
	if start = ALL then
		integer kx = 0
		start = 1
		while start with entry do
			kx += 1
			this[kx] = start
			start += 1
		entry
			start = find(that, this, start)
		end while
		this = remove(this, kx+1, length(this))
		return this
	else
		return find(that, this, start)
	end if
end function
--------------------------------------------------------------------------------
--/*
--Parameters:
--# ##this##: the source within which to search
--# ##that##: the object being looked for
--# ##what##: either the index of the start of the search [Default 1]
-- or ALL, if you want to record all matches
--
-- Returns:
--
-- either
--* an **integer**: the index of the start of ##that## in ##this##,
-- or //0// (zero) if the search is unsuccessful
--* a **sequence** containing a **vector** of all matching indices
--
--*/
--------------------------------------------------------------------------------
global function partOf(sequence this, integer where=HEAD, integer size=1)	-- returns a sub-sequence of the source
	switch where do
		case HEAD then
			return head(this, size)	-- uses Core routine
		case TAIL then
			return tail(this, size)	-- uses Core routine
		case else	-- slice from where for size
			return this[where..where+size-1]
	end switch
end function
--------------------------------------------------------------------------------
--/*
--Parameters:
--# ##this##: the source
--* ##where##: the location of the extraction - HEAD [Default], TAIL
-- or the index value of the desired start point (>=2)
--* ##size##: the number of elements to be extracted [Default = 1]
--
--Returns:
--
-- a **sequence**: containing the extracted part of the source
--*/
--------------------------------------------------------------------------------
--/*
--<built-in> <eucode>global function remove(sequence this, integer start=1, integer stop=start)-- remove[s an] item[s] from the sequence</eucode>
--
-- Parameters:
--# ##this##: the source sequence
--# ##start##: the (starting) index for removal [Default = 1]
--# ##stop##: the index at which to stop removing [Default = ##start##]
--
-- Returns:
--
-- a **sequence**: obtained by carving the ##start..stop## slice
-- out of ##this##.
--
-- Notes:
--
-- A new sequence is returned. The source can be any form of sequence.
--*/
--------------------------------------------------------------------------------
global function removeFrom(sequence this, object that, integer stop=0)	-- removes elements from this
	if stop then	-- extraction of a section of the source
		if integer(that) and that <= stop then
			return remove(this, that, stop)	-- uses Core routine
		else
			return this
		end if
	end if
	-- only that defined, so remove all 'that's
	integer found = 1
	while found entry do
		this = remove(this, found)	-- uses Core routine
	entry
		found = find(that, this, found)	-- uses Core routine
	end while
	return this
end function
--------------------------------------------------------------------------------
--/*
-- Parameters:
--
--Either:
--# ##this##: the source sequence to be operated upon
--# ##that##: the object to remove
--
--Or:
--# ##this##: the source sequence to be operated upon
--# ##that##: the index point to start removal
--# ##stop##: the index of the last element to be removed
--
-- Returns:
--
-- a **sequence** either:
--* ##this## with [##that##..##stop##] removed, or
--* of length at most ##length(this)##, with all elements containing ##that##
-- removed
--
-- Error:
--
-- If both ##that## and ##stop## are specified then they must be **integer**s
-- and sequenced such that ##stop## >= ##that##. If this is not the case then
-- ##this## is returned in its entirity.
--
-- Notes:
--
-- This function weeds out elements, not sub-sequences.
--*/
--------------------------------------------------------------------------------
global function reverse(sequence this, integer posFrom = 1, integer posTo = 0)  -- reverses the order of [some] elements
	integer n = length(this)
	if n < 2 then
		return this
	end if
	if posFrom < 1 then
		posFrom = 1
	end if
	if posTo < 1 then
		posTo = n + posTo
	end if
	if posTo < posFrom or posFrom >= n then
		return this
	end if
	if posTo > n then
		posTo = n
	end if
	integer lLimit = floor((posFrom+posTo-1)/2)	-- uses Core routine
	sequence t = this
	integer uppr = posTo
	for lowr = posFrom to lLimit do
		t[uppr] = this[lowr]
		t[lowr] = this[uppr]
		uppr -= 1
	end for
	return t
end function
--------------------------------------------------------------------------------
--/*
-- Parameters:
--# ##this##: the sequence to operate on
--# ##posFrom##: the starting point of the reversal [Default = 1]
--# ##posTo##: the end point of the reversal [Default = 0]
--
-- Returns:
--
-- a **sequence**, with the same length as ##this##
-- and with the same elements, but those with indices between
-- ##posFrom## and ##posTo## now appear in the reverse order.
--
-- Notes:
--
-- In the resulting sequence, some or all top-level elements appear in reverse
-- order relative to the original sequence.
-- This does not mean, however, that any sub-sequences found in the original
-- sequence will be reversed. (These would have to be ##reverse##d separately.)
-- 
-- The ##posTo## parameter can be negative.
-- This choice indicates an offset from the last element.
-- Thus ##-1## means the second-last element and ##0## means the last element.
--*/
--------------------------------------------------------------------------------
--/*
--====The filename object
--*/
--------------------------------------------------------------------------------
global function extensionOf(filename this)   -- : string -- the extension part of this
    sequence rev = reverse(this)
    integer dot = findIn(rev, '.')
    string ext = rev[1..dot-1]
    return reverse(ext)
end function
--------------------------------------------------------------------------------
--/*
-- Parameter:
--# ##this##: the name of the file in question
--
-- Returns:
--
-- a **string**: the file extension of ##this##
--*/
--------------------------------------------------------------------------------
override function open(filename this, threeway form=ASTEXT, integer action=TOREAD)	-- : atom - creates a handle to the file's contents
	if form = ASDLL then
		atom ret = machine_func(M_OPEN_DLL, this)   -- uses machine code routine
			if ret = 0 then
				ret = -1
			end if
		return ret
	else
		string mode = {action}
		if form = ASBINARY then
			mode &= 'b'
		end if
		return eu:open(this, mode)	-- uses Core routine
	end if
end function
--------------------------------------------------------------------------------
--/*
-- Parameters:
--# ##this##: the name of the file to open
--# ##form##: the type of access - one of ASBINARY, ASDLL, ASTEXT [Default]
--# ##action##: the action required on obtaining access - one of TOAPPEND,
-- TOREAD [Default], TOWRITE, TOUPDATE
--
-- ##ASDLL## indicates access is sought to either a dynamic link library (.dll)
-- in MS Windows, or a Unix shared library (.so). The third parameter is ignored
-- for this option.
--
-- Returns:
--
-- an **atom**: for use in accessing the file's contents,
-- according to the following.
--
-- If access is either ##ASTEXT## or ##ASBINARY## then a **filehandle** is
-- returned - see //atom.e//.
--
-- If access is set to ##ASDLL## then the resulting value is a large
-- whole-number value (a 32-bit address) which acts as a handle to a C library
-- module (a **clib** value - see //atom.e//).
--
-- Error:
--
-- A return value of //-1// signifies a failure to open the file appropriately.
--
-- This could be because the filename is incorrect or because the access
-- assigned by the operating system is in conflict with that sought by the call
-- to ##open##.
--
-- In the case of access denoted by ##ASDLL## failure could also indicate that
-- the file in question is not in any of the operating system's standard 
-- search paths.
--*/
--------------------------------------------------------------------------------
--/*
--====The string object
--*/
--------------------------------------------------------------------------------
global function changeCaseOf(string this, integer toCase)	-- change case of a string
	switch toCase do
		case TOLOWER then
			return toLower(this)
		case TOUPPER then
			return toUpper(this)
		case TOPROPER then	
			return toProper(this)
		-- more to add?
	end switch
end function
--------------------------------------------------------------------------------
--/*
--Parameters:
--# ##this##: the text to be converted
--# ##toCase##: the case change required (TOLOWER, TOPROPER or TOUPPER)
--
--Returns:
--
-- a **string**: a copy of the original but with all the characters being of the
-- selected case
--*/
--------------------------------------------------------------------------------
global function matchIn(string this, string that, integer what=1)	-- seeks [all cases of] a (sub)string
	if what = ALL then
		vector indices = EMPTY_SEQUENCE
		integer found = match(that, this)
		while found do
			indices &= found
			found = match(that, this, found+1)
		end while
		return indices
	else
		return match(that, this, what)
	end if
end function
--------------------------------------------------------------------------------
--/*
--Parameters:
--# ##this##: the source within which to search
--# ##that##: the string being looked for
--# ##what##: either the index of the start of the search [Default 1]
-- or ALL, if you want to record all matches
--
-- Returns:
--
-- either
--* an **integer**: the index of the start of ##that## in ##this##,
-- or //0// (zero) if the search is unsuccessful
--* a **sequence** containing a **vector** of all matching indices
--
--*/
--------------------------------------------------------------------------------
global function String(string prompt="", integer channel=KEYBOARD)	-- Constructor
    if length(prompt) then
		puts(SCREEN, prompt)
    end if
    object resp = gets(channel)	-- uses Core routine
    if sequence(resp) then
		if resp[$] = EOL then	-- trim the tail
			resp = resp[1..$-1]
		end if
		if string(resp) then	-- valid string has been input
			return resp
		else
			return EMPTY_SEQUENCE	-- invalid input
		end if
	else	-- must be an atom
		if resp = EOS then
			return END_OF_STREAM	-- terminator
		else
			return EMPTY_SEQUENCE	-- invalid input
		end if
    end if
end function
--------------------------------------------------------------------------------
--/*
--Parameters:
--# ##prompt##: the prompt to seek a string response [Default ""]
--# ##channel##: the device handle used for input [Default KEYBOARD]
--
-- Returns:
--
-- a **string**: either
--* the line of text input, less any line-feed
--* ##EMPTY_SEQUENCE## - for invalid input
--* ##END_OF_STREAM## - to signify the stream terminator
--
-- Notes:
--
-- If ##prompt## is other than "" then it is issued before the input string is
-- obtained, thus acting as a prompt to guide the user as to what is being
-- sought in a particular case.
--*/
--------------------------------------------------------------------------------
global function trim(string this, threeway how=ENDS, object these=" \t\r\n")	-- trims characters from string
	if atom(these) then
		these = {these}
	end if
	integer lpos = 1
	if how = HEAD or how = ENDS then
		while lpos <= length(this) do
			if not find(this[lpos], these) then
				exit
			end if
			lpos += 1
		end while
	end if
	integer rpos = length(this)
	if how = TAIL or how = ENDS then
		while rpos > lpos do
			if not find(this[rpos], these) then
				exit
			end if
			rpos -= 1
		end while
	end if
	return this[lpos..rpos]
end function
--------------------------------------------------------------------------------
--/*
-- Parameters:
--# ##source##: the source
--# ##how##: the end[s] at which to trim - ENDS[Default], HEAD or TAIL
--# ##these##: the set of item to trim [Default " \t\r\n"]
--
-- Returns:
--
-- a **string**: the trimmed version of ##source##
--*/
--------------------------------------------------------------------------------
--/*
--====The vector object
--*/
--------------------------------------------------------------------------------
global function meanOf(vector this, threeway typ=ARITH)	-- the [ARITH|GEOM|HARM] mean of the vector's values
	atom len = length(this)	-- uses Core routine
	switch typ do
		case ARITH then
			return sumOf(this)/len
		case GEOM then
			return power(productOf(this), 1/len)	-- uses Core routine
		case HARM then    
			return len/sumOf(power(this, -1))	-- uses Core routine
	end switch
end function
--------------------------------------------------------------------------------
--/*
--Parameter:
--# ##this##: the source vector
--# ##typ##: the type of mean required - ARITH[Default], GEOM or HARM
--
--Returns:
--
-- an **atom**: the chosen mean of all the entries
--*/
--------------------------------------------------------------------------------
global function allOred(vector this)	-- [atom] ors all the elements
	atom ret = 0
	for i = 1 to length(this) do
		ret = or_bits(ret, this[i])
	end for
	return ret
end function
--------------------------------------------------------------------------------
--/*
--Parameter:
--# ##this##: the source vector
--
--Returns:
--
-- an **atom**: the result of or'ing all the entries
--*/
--------------------------------------------------------------------------------
global function productOf(vector this)	-- the result of multiplying all elements together
	atom ret = 1
	for i = 1 to length(this) do
		ret *= this[i]
	end for
	return ret
end function
--------------------------------------------------------------------------------
--/*
--Parameter:
--# ##this##: the source vector
--
--Returns:
--
-- an **atom**: the product of all the entries
--*/
--------------------------------------------------------------------------------
global function sumOf(vector this)    -- adds up all the elements
    atom total = 0
    for i = 1 to length(this) do
        total += this[i]
    end for
    return total
end function
--------------------------------------------------------------------------------
--/*
--Parameter:
--# ##this##: the vector to be summed
--
--Returns:
--
-- an **atom**: the sum of all the entries
--*/
--------------------------------------------------------------------------------
global function Vector(object this, integer op=ARITH, integer increment=1, integer count=2)	-- creates vector
	if count <= 0 then
		return EMPTY_SEQUENCE
	end if
	if sequence(this) then
		if vector(this) then
			return this
		else
			return EMPTY_SEQUENCE
		end if
	end if
	vector result = repeat(0, count)-- this must be an atom to have reached here
	result[1] = this
	switch op do
		case ARITH then
			for i = 2 to count do
				this += increment
				result[i] = this
			end for
		case GEOM then
			for i = 2 to count do
				this *= increment
				result[i] = this
			end for
		case else
			return EMPTY_SEQUENCE
	end switch
	return result
end function
--------------------------------------------------------------------------------
--/*
-- Parameters:
--# ##this##: either the desired source **vector**, or
-- the initial (**atom**) value from which to start a series
--# ##op##: the type of operation used to build the series: either
-- ARITH for a linear series [Default] or GEOM for a geometric series
--# ##increment##: the value to use recursively on ##this## to generate new
-- elements [Default 1]
--# ##count##: the length of the series [Default 2]
--
-- Returns:
--
--a **vector**, either
--* ##this##, after type-checking
--* ##EMPTY_SEQUENCE##, where some form of error occurred, or
--* a **vector** containing the specified series.
-- 
-- Notes:
--
-- To enter your own **vector**, make this your first and only parameter.
-- It will be type-checked in the normal way.
--
-- To create a series **vector**, define the starting (**atom**)
-- value as ##this## and set any other parameters where the defaults do not
-- apply.
--
-- In cases where a series has successfully been created, the following apply:
--
-- * the first item is always ##this##
-- * an ##ARITH##metic series formed by **adding** ##increment##s
-- * a ##GEOM##etric// series formed by **multiplying** ##increment##s
--
-- The resulting **vector** will be of length ##count+1##,
-- starting with ##this## and with adjacent elements differing
-- by ##increment##.
--
-- Errors:
--
-- If ##count## is zero or negative, or if any of ##start##, ##op##
-- or ##increment## is invalid, then ##EMPTY_SEQUENCE## is returned.
--*/
--------------------------------------------------------------------------------
--/*
--==== Defined instances
---/
--------------------------------------------------------------------------------
global constant EMPTY_SEQUENCE = {}
--------------------------------------------------------------------------------
-- Previous versions
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.30
--Euphoria Versions: 4.0.5 upwards; built with v4.1.0 [64bit]
--Author: C A Newbould
--Date: 2020.08.03
--Status: incomplete; operational
--Changes:]]]
--* ##matchIn## (**string**) defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.29
--Euphoria Versions: 4.0.5 upwards; built with v4.1.0 [64bit]
--Author: C A Newbould
--Date: 2020.01.08
--Status: incomplete; operational
--Changes:]]]
--* ##allOred## defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.28
--Euphoria Versions: 4.0.5 upwards; built with v4.1.0 [64bit]
--Author: C A Newbould
--Date: 2020.01.07
--Status: incomplete; operational
--Changes:]]]
--* ##write## moved to //object.e//
--* associated constants also moved
--* use of ##write## internally removed
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.27
--Euphoria Versions: 4.0.5 upwards; built with v4.1.0 [64bit]
--Author: C A Newbould
--Date: 2020.01.02
--Status: incomplete; operational
--Changes:]]]
--* documentation revised
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.26
--Euphoria Versions: 4.0.5 upwards; built with v4.1.0 [64bit]
--Author: C A Newbould
--Date: 2019.12.21
--Status: incomplete; operational
--Changes:]]]
--* ##convertToC## removed
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.25
--Euphoria Versions: 4.0.5 upwards; built with v4.1.0 [64bit]
--Author: C A Newbould
--Date: 2019.12.21
--Status: incomplete; operational
--Changes:]]]
--* modified so that it does not depend on //object.e//
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.24
--Euphoria Versions: 4.0.5 upwards; built with v4.1.0 [64bit]
--Author: C A Newbould
--Date: 2019.12.20
--Status: incomplete; operational
--Changes:]]]
--* ##convertToC## defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.23
--Euphoria Versions: 4.0.5 upwards; built with v4.1.0 [64bit]
--Author: C A Newbould
--Date: 2019.12.19
--Status: incomplete; operational
--Changes:]]]
--* ##open## re-defined and extended
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.22
--Euphoria Versions: 4.0.5 upwards; built with v4.1.0 [64bit]
--Author: C A Newbould
--Date: 2019.12.18
--Status: incomplete; operational
--Changes:]]]
--* ##extensionOf## defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.21
--Euphoria Versions: 4.0.5 upwards; built with v4.1.0 [64bit]
--Author: C A Newbould
--Date: 2019.12.18
--Status: incomplete; operational
--Changes:]]]
--* ##readFrom## removed - conflict
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.20
--Euphoria Versions: 4.0.5 upwards; built with v4.1.0 [64bit]
--Author: C A Newbould
--Date: 2019.12.13
--Status: incomplete; operational
--Changes:]]]
--* ##changeCaseOf## extended - TOPROPER
--* defined local routine (needed for recursion)
--* defined corresponding local routines for TOLOWER and TOUPPER
--* extended ducumentation
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.19
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.12.12
--Status: incomplete; operational
--Changes:]]]
--* ##readFrom## created
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.18
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.12.11
--Status: incomplete; operational
--Changes:]]]
--* ##filename## created
--* ##open## documented
--* introductory documentation extended
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.17
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.12.08
--Status: incomplete; operational
--Changes:]]]
--* ##remove## documented
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.16
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.12.07
--Status: incomplete; operational
--Changes:]]]
--* ##removeFrom## defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.15
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.12.03
--Status: incomplete; operational
--Changes:]]]
--* ##findIn## modified and extended
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.14
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.12.01
--Status: incomplete; operational
--Changes:]]]
--* ##findIn## defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.13
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.29
--Status: incomplete; operational
--Changes:]]]
--* ##write## extended
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.12
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.28
--Status: incomplete; operational
--Changes:]]]
--* ##String## extended
--* //threeway// extended
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.11
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.28
--Status: incomplete; operational
--Changes:]]]
--* ##partOf## defined
--* ##write## modified
--* ##HEAD## value changed
--* ##reverse## edited in a minor way
--* ##Vector## defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.10
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.23
--Status: incomplete; operational
--Changes:]]]
--* ##String## created
--* ##sum## renamed
--* ##product## renamed
--* ##mean## renamed
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.9
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.22
--Status: incomplete; operational
--Changes:]]]
--* ##BOTH## changed to ##ENDS##
--* minor editing changes to names
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.8
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.21
--Status: incomplete; operational
--Changes:]]]
--* ##changeCaseOf## (string) defined
--* ##upperCase## absorbed
--* ##lowerCase## absorbed
--* re-ordered initial documentation
--* ##mean## - use of ##switch## replacing ##if##
--* modified definition of isCharacter
--* ##write## modified
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.7
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.17
--Status: incomplete; operational
--Changes:]]]
--* ##addTo## (string) simplified
--* ##AFTER## and ##BEFORE## modified again
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.6
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.16
--Status: incomplete; operational
--Changes:]]]
--* ##addTo## (string) extended
--* ##AFTER## and ##BEFORE## modified
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.5
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.14
--Status: incomplete; operational
--Changes:]]]
--* ##upperCase## (string) defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.4
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.13
--Status: incomplete; operational
--Changes:]]]
--* ##trim## (string) defined
--* threeway defined to support ##trim##
--* ##lowerCase## (string) defined
--* documentation re-ordered
--* allTheSame extended
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.3
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.12
--Status: incomplete; operational
--Changes:]]]
--* ##mean## (vector) defined
--* ##threeway## defined internally
--* ##product## defined
--* ##prependTo## defined
--* ##appendTo## and ##prependTo## merged, as ##addTo##
--* modified constants to enums
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.2
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.11
--Status: incomplete; operational
--Changes:]]]
--* ##reverse## (sequence) defined
--* ##EMPTY_SEQUENCE## defined
--* ##sum## (vector) defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.1
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.11
--Status: incomplete; operational
--Changes:]]]
--* ##appendTo## (sequence) defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.0
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.11
--Status: incomplete; operational
--Changes:]]]
--* created
--* **string** defined
--* **vector** defined
--* ##write## (string) defined
--------------------------------------------------------------------------------
