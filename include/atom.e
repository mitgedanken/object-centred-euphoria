--------------------------------------------------------------------------------
--	Library: atom.e
--------------------------------------------------------------------------------
-- Notes:
--
-- 
--------------------------------------------------------------------------------
--/*
--= Library: (euphoria410_64)(include)(oce)atom.e
-- Description: The atom library module for Object-Centred Ephoria.
------
--[[[Version: 4.1.0.16
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2021.01.15
--Status: incomplete; operational
--Changes:]]]
--* //public// changed to //global//
--
--==Object-Centred Euphoria library: atom
--
-- This library hold all the atom-based functionality in Object-Centred
-- Euphoria.
--
-- This is a key extension module for OCE and must be included
-- directly, or indirectly, in all OCE modules, both user-defined libraries and
-- applications, which need either **atom**s or any of their more
-- specialised derivatives.
--
-- This is usually achieved automatically by calling the //object.e// module.
--
-- The following are part of the OCE Atom Library Extension to Core Euphoria.
--
--===Constants
--* //ASASTRING//
--* //ASFUNC//
--* //ASLINES//
--* //ASPROC//
--* //BYTES//
--* //C_ATOM//
--* //C_BOOL//
--* //C_DWORD//
--* //C_HANDLE//
--* //C_HWND//
--* //C_INT//
--* //C_LONG//
--* //C_LPARAM//
--* //C_LPSTR//
--* //C_LRESULT//
--* //C_POINTER//
--* //C_UINT##
--* //C_WPARAM//
--* //LINE//
--===Types (and their routines)
--* **address**(**atom**)
--** ##Address##(integer)   :   address
--** ##free##(address)
--* **atom**(built-in)
--** ##abs##(atom)   :   atom
--** ##isPositive##(atom)  :   boolean
--** ##sign##(atom)  :   integer
--* **boolean**(**integer**)
--** ##iif##(boolean, object, object)    :   object
--* **byte**(**integer**)
--* **clib**(**atom**)
--** ##define##(clib, string[, sequence][, atom] :   vector
--* **crid**(**atom**)
--** ##execC(crid[, sequence, boolean]    :    object
--* **filehandle**(**integer**)
--** ##flushBuffer##(filename)
--** ##positionIn##(filename, integer)	:	boolean
--** ##readFrom##(filename[, integer])    :   object
--** ##whereIn##(filename)	:	integer
--
-- Utilise this support by adding the following statement to your module:
-- <eucode>include oce/atom.e</eucode>
-- or, more generally, using:
-- <eucode>include oce/object.e</eucode>
------
--*/
--------------------------------------------------------------------------------
--/*
--==Interface
--*/
--------------------------------------------------------------------------------
--
--=== Includes
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--/*
--=== Constants
--*/
--------------------------------------------------------------------------------
--	Local
--------------------------------------------------------------------------------
constant CHUNK = 100
constant EOF = -1
constant M_ALLOC = 16
constant M_DEFINE_C = 51
constant M_FLUSH = 60
constant M_FREE = 17
constant M_SEEK = 19
constant M_WHERE = 20
constant NULL = 0
constant NOTOPEN = -1
--------------------------------------------------------------------------------
--	Shared with other modules
--------------------------------------------------------------------------------
global constant ASFUNC = (1=0)
global constant ASPROC = (1=1)
global enum BYTES, LINE, ASLINES, ASASTRING     -- for open
global constant C_ATOM = #02000002
global constant C_BOOL = #01000004
global constant C_DWORD = #02000004
global constant C_HANDLE = #03000002
global constant C_HWND = #03000002
global constant C_INT = C_BOOL
global constant C_LONG = C_INT
global constant C_LPARAM = #03000001
global constant C_LPSTR = C_LPARAM
global constant C_LRESULT = C_LPARAM
global constant C_POINTER = C_LPARAM
global constant C_UINT = C_DWORD
global constant C_WPARAM = C_LPARAM
global constant FALSE = (1=0)
global constant TRUE = (1=1)
--------------------------------------------------------------------------------
--/*
--=== Euphoria types
--*/
--------------------------------------------------------------------------------
--	Local
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--	Shared with other modules
--------------------------------------------------------------------------------
global type address(atom this)  -- a memory address
    return this > 0
end type
--------------------------------------------------------------------------------
global type boolean(integer this)   -- FALSE or TRUE
    return this = FALSE or this = TRUE
end type
--------------------------------------------------------------------------------
global type byte(integer this)  -- integer between 0 and 255
    return this > EOF and this < 256
end type
--------------------------------------------------------------------------------
global type clib(atom this) -- handle to a dll/so
    return this = NOTOPEN or this > 0
end type
--------------------------------------------------------------------------------
global type crid(atom this)    -- pointer to a routine in a C library module; return value C-type
    return this = NOTOPEN or this > 0
end type
--------------------------------------------------------------------------------
global type filehandle(integer this)    -- a valid file handle (3+) or NOTOPEN
    return this = NOTOPEN or this > 2
end type
--------------------------------------------------------------------------------
--
--=== Variables
--
--------------------------------------------------------------------------------
--	Local
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--	Shared with other modules
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--/*
--=== Routines
--*/
--------------------------------------------------------------------------------
--/*
--==== address-based routines
--*/
--------------------------------------------------------------------------------
--	Local
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--	Shared with other modules
--------------------------------------------------------------------------------
global function Address(integer this)  -- {address] allocates n bytes of memory
    if this > 0 then
        return machine_func(M_ALLOC, this)
    else
        return NULL
    end if
end function
--------------------------------------------------------------------------------
--/*
--Parameter:
--# //this//: the number of contiguous bytes to reserve for the address
--
--Returns:
--
--an **address*: the pointer to the memory block
--
--Error:
--
--The return value is set to NULL if //this// is not positive.
--*/
--------------------------------------------------------------------------------
global procedure free(address this) -- frees the memory at a given address
    machine_proc(M_FREE, this)
end procedure
--------------------------------------------------------------------------------
--/*
--Parameter:
--# ##this##: the address of a block of previously allocated memory
--
--Notes:
--
-- When a program terminates, Euphoria will return all allocated memory
-- to the system automatically.
--*/
--------------------------------------------------------------------------------
--/*
--==== atom-based routines
--*/
--------------------------------------------------------------------------------
--	Local
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--	Shared with other modules
--------------------------------------------------------------------------------
global function abs(atom this)  -- absolute value
    return sign(this) * this
end function
--------------------------------------------------------------------------------
--/*
--Parameter:
--# ##this##: the value to be scrutinised
--
--Returns:
--
-- an **atom**: the absolute value of ##this##
--*/
--------------------------------------------------------------------------------
global function isPositive(atom this)   -- TRUE if positive
    return this > 0
end function
--------------------------------------------------------------------------------
--/*
--Parameter:
--# ##this##: the value to be scrutinised
--
--Returns:
--
-- a **boolean**: TRUE, if ##this## is > 0
--*/
--------------------------------------------------------------------------------
global function sign(atom this) -- sign * 1
    if this > 0 then
        return 1
    elsif this = 0 then
        return 0
    else
        return -1
    end if
end function
--------------------------------------------------------------------------------
--/*
--Parameter:
--# ##this##: the value to be scrutinised
--
--Returns:
--
-- an **integer**: //1//, //0// or //-1//, depending on the sign of ##this##
--*/
--------------------------------------------------------------------------------
--/*
--==== boolean-based routines
--*/
--------------------------------------------------------------------------------
export function iif(boolean this, object ifTrue, object ifFalse)    -- an 'if' test inside an expression
	if this then
		return ifTrue
	end if
	return ifFalse
end function
--------------------------------------------------------------------------------
--/*
-- Parameters:
--# ##this##: a boolean expression
--# ##ifTrue##: the value to be returned if ##this## is ##TRUE##
--# ##ifFalse##: the value to be returned if ##this## is ##FALSE##
--
-- Returns:
--
-- an **object**:
-- either ##ifTrue## or ##ifFalse## depending on ##this##.
--*/
--------------------------------------------------------------------------------
--/*
--==== clib-based routines
--*/
--------------------------------------------------------------------------------
--	Local
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--	Shared with other modules
--------------------------------------------------------------------------------
global function define(clib in, sequence name, sequence taking={}, integer returning=0) -- : crid - sets the handle to the C routine
    return machine_func(M_DEFINE_C, {in, name, taking, returning})
end function
--------------------------------------------------------------------------------
--/*
--Parameters:
--# ##in##: the handle to the dll or so module (as returned via ##open##
-- with the ##ASDLL## option
--# ##name##: the (string) value holding the name of the routine in the library
--# ##taking##: a list of the C-language constants defining the type[s] of arguments
-- the routine requires
--# ##returning##: the C-language constant defining the type of the return value 
-- [Default = VOID]
--
-- Returns:
--
-- a **crid**:a handle to the routine, to act as an ID for calling it later
--
-- Error:
--
-- If the routine, including its full signature profile, cannot be located in
-- the C module then a value of //-1// (not found) is returned .
--*/
--------------------------------------------------------------------------------
--/*
--==== crid-based routines
--*/
--------------------------------------------------------------------------------
--	Local
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--	Shared with other modules
--------------------------------------------------------------------------------
global function execC(crid this, sequence params={}, boolean asproc = ASFUNC) -- : object - executes the C-function defined by this, with set parameters
    switch asproc do
        case ASFUNC then
            return c_func(this, params) -- uses built-in function
        case ASPROC then
            c_proc(this, params) -- uses built-in function
            return 0    
    end switch
end function
--------------------------------------------------------------------------------
--/*
--Paramteters:
--# ##this##: the value returned from ##define##
--# ##params##: the set of desired parameter values
--# ##asproc##: a flag to indicate whether the routine has a return value - 
-- either //ASFUNC// [Default] or //ASPROC//
--
--Returns:
--
-- an **object**: the return value determined by the C-function's definition,
-- or //0//, if the C-function is void.
--*/
--------------------------------------------------------------------------------
--/*
--==== filehandle-based routines
--*/
--------------------------------------------------------------------------------
global procedure flushBuffer(filehandle this)   -- empties the handle's buffer
	machine_proc(M_FLUSH, this)
end procedure
--------------------------------------------------------------------------------
--/*
--Parameter:
--# ##this##: the handle to the file being processed (low-level)
--
--*/
--------------------------------------------------------------------------------
global function positionIn(filehandle this, integer where) --  : boolean - moves file's internal pointer
    return not machine_func(M_SEEK, {this, where})
end function
--------------------------------------------------------------------------------
--/*
--Parameter:
--# ##this##: the handle to the file being processed (low-level)
--# ##where##: the index of the location of the file's internal pointer
--
--Returns:
--
-- a **boolean**: ##TRUE## if movement of pointer is successful
--
-- Notes:
--
-- If ##where## is set to //-1// then the pointer is moved to the end of the
-- file's contents
--*/
--------------------------------------------------------------------------------
global function readFrom(filehandle this, integer typ = ASASTRING, integer number = 1)  --  : object - reads value[s] according to typ
    switch typ do
        case BYTES then   -- read times bytes
            if number = 1 then
                return getc(this)   -- uses core routine
            else    
                sequence s = {}
                for i = 1 to number do
                    integer c = getc(this)
                    if c = EOF then
                        return s
                    else
                        s = append(s, c)
                    end if
                end for
                return s
            end if
        case LINE then
            object ret = gets(this)   -- uses core routine
            if sequence(ret) and ret[$] = '\n' then
                ret = ret[1..$-1]
                if ret[$] = '\r' then
                    ret = ret[1..$-1]
                end if
            end if
            return ret
        case ASLINES then
            sequence ret = {}
            positionIn(this, 0)   -- go to start
            object y
            --read all the file's lines
            while sequence(y) with entry do
                if y[$] = '\n' then
                    y = y[1..$-1]
                    ifdef UNIX then
                        if length(y) then
                            if y[$] = '\r' then
                                y = y[1..$-1]
                            end if
                        end if
                    end ifdef
                end if
                ret = append(ret, y)
            entry
                y = gets(this)   -- uses core routine
            end while
            --return the sequence of lines
            return ret
        case ASASTRING then
            sequence str = ""
            positionIn(this, 0)   -- go to start
            --read all the file's bytes
            integer ch
            while ch != EOF with entry do
                str &= ch
            entry
                ch = getc(this)   -- uses core routine
            end while
            --all characters bar e-o-f
            return str
    end switch
end function
--------------------------------------------------------------------------------
--/*
--Parameter:
--# ##this##: the handle to the file being accessed
--# ##typ##: the type of data extraction to be made,
-- either:
--* BYTES - to read a number of characters from the current pointer position
--* LINE - to read the next line of data [to the next incidence of '\n' or the
-- end-of-file marker] from the current pointer position
--* ASLINES - to read all the file's contents as lines of separate strings
--* ASASTRING - to read all the file's contents as a sequence of characters
--* an **integer** > 0 - to read the next ##typ## characters from the current
-- pointer position
--# [##number##: the number of bytes to read, if BYTES chosen
--
--Returns:
--
-- an **object**: whose type depends on the kind of extraction selected
-- and on the position of the pointer in the file when ##read## is called:
-- either:
--* a **byte**, if ##BYTES## selected and ##number## = 1
--* a **sequence**, holding a number of bytes, if ##typ## = ##BYTES## and
-- ##number## > 1
--* the end-of-file marker (##EOF## = //-1//), if LINE selected with the pointer
-- at ##EOF##
--* a **string**, if LINE selected and the pointer not at ##EOF##
--* a **sequence**, each element containing a string of characters,
-- if ASLINES selected
--* a **string**, if ASASTRING selected
--
-- Note:
--
-- This function operates at both low and high levels, so you can either extract
-- all the contents of the file, or extract just the next few characters in
-- the file.
--
-- Options ##ASLINES## and ##ASSTRING## both move the pointer to the start of
-- the file before reading; all other options operate from the current position
-- in the file, as indicated by ##whereIn##, and as controlled by
-- ##positionIn##.
--*/
--------------------------------------------------------------------------------
global function whereIn(filehandle this)    -- : integer - where in the file the pointer is
    return machine_func(M_WHERE, this)
end function
--------------------------------------------------------------------------------
--/*
--Parameter:
--# ##this##: the handle to the file being processed (low-level)
--
--Returns:
--
-- an **integer**: the position of the pointer
--*/
--------------------------------------------------------------------------------
--
--==== Defined instances
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Previous versions
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.15
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2021.01.14
--Status: incomplete; operational
--Changes:]]]
--* ##read## renamed as ##readFrom##
--* //C_LONG// defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.14
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.02.09
--Status: incomplete; operational
--Changes:]]]
--* ##read## renamed as ##readFrom##
--* //ASFUNC// defined
--* //ASPROC// defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.13
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.01.09
--Status: incomplete; operational
--Changes:]]]
--* **boolean** moved from //object.e//
--* **crid** reverted to old definition
--* ##execC## revised
--* //ASFUNC// defined
--* //ASPROC// defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.12
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.01.08
--Status: incomplete; operational
--Changes:]]]
--* //C_LPSTR// defined
--* //C_BOOL// defined
--* ##Address## defined
--* //C_DWORD// defined
--* ##define## extended
--* **crid** re-defined
--* //C_ATOM// defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.11
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.01.07
--Status: incomplete; operational
--Changes:]]]
--* //C_HWND// defined
--* //C_WPARAM// defined
--* //C_LPARAM// defined
--* //C_LRESULT// defined
--* //C_HANDLE// defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.10
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.01.03
--Status: incomplete; operational
--Changes:]]]
--* **clib** made public
--* ##C_UINT## defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.9
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2020.01.02
--Status: incomplete; operational
--Changes:]]]
--* documentation revised
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.8
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.12.30 
--Status: incomplete; operational
--Changes:]]]
--* ##abs## defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.7
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.12.21 
--Status: incomplete; operational
--Changes:]]]
--* ##CTostring## removed
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.6
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.12.21 
--Status: incomplete; operational
--Changes:]]]
--* modified so that it does not depend on //object.e//
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.5
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.12.20
--Status: incomplete; operational
--Changes:]]]
--* details not recorded
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.4
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.12.19 
--Status: incomplete; operational
--Changes:]]]
--* **clib** defined
--* **crid** defined
--* ##define## defined
--* ##C_INT## defined
--* ##C_POINTER## defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.3
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.12.17 
--Status: incomplete; operational
--Changes:]]]
--* ##read## defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.2
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.12.15 
--Status: incomplete; operational
--Changes:]]]
--* **filehandle** defined
--* ##positionIn## defined
--* ##whereIn## defined
--* ##flushBuffer## defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.1
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.12.07 
--Status: incomplete; operational
--Changes:]]]
--* created
--* ##sign## defined
--------------------------------------------------------------------------------
--[[[Version: 4.1.0.64.0
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2019.11.12 
--Status: incomplete; operational
--Changes:]]]
--* created
--* **byte** defined
--* ##isPositive## defined
--------------------------------------------------------------------------------
