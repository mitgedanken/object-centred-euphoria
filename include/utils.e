/*
=A set of utilities to support the Object-Centred IUP Euphoria Library

The routines enable the IUP routine definitions to be single-liners. It
is sufficiently generalised for use with other C-library
wrapper functions.

==Interface

To activite this module in a calling module ensure that

//include utils.e//

is incorporated into the code.

===Libraries

*/

public include object.e -- access the base OCE facilities

/*


===Constants

*/

export constant NULL = 0

/*


===Functions

The first two functions turn C-library wrappers into one-liners.

The first "converts" a C-library function (//void// or otherwise) into
an OCE one.

*/

export function doC(clib library, string functionname, sequence cargs = {}, sequence eargs = {}, atom cret = NULL)
	crid this = define(library, functionname, cargs, cret)
    return execC(this, eargs, (cret = NULL))
end function

/*


The second works in the specialised case where the C-language function
takes a dynamically-determined null-terminated array of pointers as its
arguments.

*/

export function doCN(clib library, string functionname, integer ctype, sequence eargs = {}, atom cret)
	sequence cargs = repeat(ctype, lengthOf(eargs) + 1)
	return doC(library, functionname, cargs, nullTail(eargs), cret)
end function

/*

This function provides a null-terminated array.
*/

export function nullTail(sequence this) -- appends a NULL
	return addTo(this, NULL, AFTER)
end function

/*

Several IUP creator functions allow for null values as alternatives to
strings. This function helps address that.
*/

export function toCOrNot(object this)	-- converts to null-terminated string if a string
	if atom(this) then return this
	else return convert(this, TOC) end if
end function

/*


==Library details

This library (OCE utils) has the following details:

* Version: 4.1.0.2
* Author: C A Newbould
* Date: 2020.05.05
* Status: operational; incomplete
* Changes:
    ** corrected error in ##doCN##

* Version: 4.1.0.1
* Author: C A Newbould
* Date: 2020.05.05
* Status: operational; incomplete
* Changes:
    ** simplified ##doC##

* Version: 4.1.0.0
* Author: C A Newbould
* Date: 2020.05.04
* Status: operational; incomplete
* Changes:
	** created
	** ##doC## defined
	** ##doCN## defined
	** ##nullTail## defined
	** ##toCOrNot## defined


*/
